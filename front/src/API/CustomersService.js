import axios from "axios";

export default class CustomersService {
  static async getAll() {
    const response = await axios.get("http://localhost:3000/customers");
    return response;
  }

  static async add(name, bonus_programm, phone_number, bonus_card_number) {
    const response = await axios
      .post(`http://localhost:3000/customers`, {
        name,
        bonus_programm,
        phone_number,
        bonus_card_number,
      })
      .catch(function (error) {
        if (error.response.status === 500) {
          return alert("Customer with this card number already exists");
        }
      });
    return response;
  }

  static async delete(id) {
    const response = await axios
      .delete(`http://localhost:3000/customers/${id}`)
      .catch(function (error) {
        if (error.response.status === 500) {
          return alert("Can't delete this customer");
        }
      });
    return response;
  }
}
