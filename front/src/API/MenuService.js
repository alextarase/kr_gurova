import axios from "axios";

export default class MenuService {
  static async getAll() {
    const response = await axios.get("http://localhost:3000/menu");
    return response;
  }

  static async getAllDishesByGroups(id) {
    console.log("id in req", id);
    const response = await axios.get(
      `http://localhost:3000/menu?groupMenu=${id}`
    );
    return response;
  }

  static async getAllGroupsMenu() {
    const response = await axios.get("http://localhost:3000/menu/groupMenu");
    return response;
  }

  static async add(name, cost, colories, weight, description, groupMenu) {
    const response = await axios
      .post(`http://localhost:3000/menu`, {
        name,
        cost,
        colories,
        weight,
        description,
        groupMenu,
      })
      .catch(function (error) {
        if (error.response.status === 404) {
          return alert("Create a group menu first");
        }
      });
    return response;
  }

  static async addGroupMenu(name) {
    const response = await axios.post(
      `http://localhost:3000/menu/createGroupMenu`,
      {
        name,
      }
    );
    return response;
  }

  static async delete(id) {
    const response = await axios
      .delete(`http://localhost:3000/menu/${id}`)
      .catch(function (error) {
        if (error.response.status === 500) {
          return alert("Can't delete this menu");
        }
      });
    return response;
  }
}
