import axios from "axios";

export default class StaffService {
  static async getAll() {
    const response = await axios.get("http://localhost:3000/staff");
    return response;
  }

  static async add(name, salary, phone_number) {
    const response = await axios.post(
      `http://localhost:3000/staff`,
      {
        name,
        salary,
        phone_number,
      },
      { headers: { Authorization: `Bearer ${localStorage.getItem("token")}`,  } }
    );
    return response;
  }

  static async delete(id) {
    console.log(id);
    const response = await axios
      .delete(`http://localhost:3000/staff/${id}`)
      .catch(function (error) {
        if (error.response.status === 500) {
          return alert("Can't delete staff, while it's working");
        }
      });
    return response;
  }

  static async patch(id, position) {
    console.log(id);
    const response = await axios.patch(`http://localhost:3000/staff/${id}`, {
      position,
    });
    return response;
  }
}
