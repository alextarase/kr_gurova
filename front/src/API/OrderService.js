import axios from "axios";

export default class OrderService {
  static async getAll() {
    const response = await axios.get("http://localhost:3000/order");
    return response;
  }

  static async getAllOrderContent() {
    const response = await axios.get(
      "http://localhost:3000/order/orderContent"
    );
    return response;
  }

  static async getAllOrderContentbyOrderID(id) {
    const response = await axios.get(
      `http://localhost:3000/order/orderContent?orderID=${id}`
    );
    return response;
  }

  static async add(
    name,
    number,
    payed_summ,
    Date_time_start,
    Date_time_finish,
    staffID,
    boardID,
    customersID
  ) {
    const response = await axios.post(`http://localhost:3000/order`, {
      name,
      number,
      payed_summ,
      Date_time_start,
      Date_time_finish,
      staffID,
      boardID,
      customersID,
    });
    return response;
  }

  static async addOrderContent(
    number,
    Date_time_start_coocking,
    Date_time_order_done,
    Date_time_order_accept,
    Date_time_order_served,
    staffID,
    orderID,
    menuID
  ) {
    const response = await axios.post(
      `http://localhost:3000/order/orderContent`,
      {
        number,
        Date_time_start_coocking,
        Date_time_order_done,
        Date_time_order_accept,
        Date_time_order_served,
        staffID,
        orderID,
        menuID,
      }
    );
    return response;
  }

  static async delete(id) {
    console.log(id);
    const response = await axios
      .delete(`http://localhost:3000/order/${id}`)
      .catch(function (error) {
        if (error.response.status === 500) {
          return alert("Can't delete this order");
        }
      });
    return response;
  }

  static async patchOrderStatus(id, status) {
    console.log("id - ", id, "status - ", status);
    const response = await axios.patch(
      `http://localhost:3000/order/${id}/status`,
      {
        status,
      }
    );
    return response;
  }
}
