import axios from "axios";

export default class BoardService {
  static async getAll() {
    const response = await axios.get("http://localhost:3000/board");
    return response;
  }

  static async getAllOrders() {
    const response = await axios.get("http://localhost:3000/board/boardOrder");
    return response;
  }

  static async getAllOrdersByboard(id) {
    const response = await axios.get(
      `http://localhost:3000/board/boardOrder?boardID=${id}`
    );
    return response;
  }

  static async add(number, sit_number) {
    const response = await axios.post(`http://localhost:3000/board`, {
      number,
      sit_number,
    });
    return response;
  }

  static async addBoardOrder(
    Date_time_ordered,
    name,
    phone_number,
    sit_number,
    boardID
  ) {
    const response = await axios.post(
      `http://localhost:3000/board/boardOrder`,
      {
        Date_time_ordered,
        name,
        phone_number,
        sit_number,
        boardID,
      }
    );
    return response;
  }

  static async delete(id) {
    const response = await axios
      .delete(`http://localhost:3000/board/${id}`)
      .catch(function (error) {
        if (error.response.status === 500) {
          return alert("Can't delete this board");
        }
      });
    return response;
  }

  static async patchBoardStatus(id, status) {
    console.log("id - ", id, "status - ", status);
    const response = await axios.patch(`http://localhost:3000/board/${id}`, {
      status,
    });
    return response;
  }
}
