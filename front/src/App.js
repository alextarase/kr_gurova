import { BrowserRouter } from "react-router-dom";
import "./styles/App.css";
import Navbar from "./components/UI/navbar/Navbar";
import AppRouter from "./components/AppRouter";
import { AuthContext } from "./context";
import { useState } from "react";
import { observer } from "mobx-react";

function App() {
  const [isAuth, setIsAuth] = useState(false);
  const [lang, setLang] = useState("RU");
  const [modal, setModal] = useState(false);
  const [modal2, setModal2] = useState(false);
  const [modal3, setModal3] = useState(false);
  const [fetching, setFetching] = useState(false);

  return (
    <AuthContext.Provider
      // @ts-ignore
      value={{
        isAuth,
        setIsAuth,
        modal,
        setModal,
        modal2,
        setModal2,
        modal3,
        setModal3,
        fetching,
        setFetching,
        lang,
        setLang,
      }}
    >
      <BrowserRouter>
        <Navbar />
        <AppRouter />
      </BrowserRouter>
    </AuthContext.Provider>
  );
}

export default observer(App);
