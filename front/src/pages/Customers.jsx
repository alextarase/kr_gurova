import React, { useContext, useEffect, useState } from "react";
import CustomersService from "../API/CustomersService";
import CustomersList from "../components/CustomerComponents/CustomersList";
import { AuthContext } from "../context";
import MyModal from "./../components/UI/MyModal/MyModal";
import CustomersForm from "./../components/CustomerComponents/CustomersForm";
import GreenButton from "./../components/UI/button/GreenButton";

const Customers = () => {
  const [customers, setCustomers] = useState([]);

  const { modal, setModal } = useContext(AuthContext);
  const { fetching, setFetching } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);

  async function fetchCustomers() {
    const response = await CustomersService.getAll();
    setCustomers(response.data);
    console.log("customers log", response.data);
  }

  // Отрисовывает клиентов при загрузке страницы
  useEffect(() => {
    fetchCustomers();
  }, []);

  // Получаем клиентов при добавлении или удалении клиентов
  useEffect(() => {
    fetchCustomers();
    setFetching(false);
  }, [fetching, setFetching]);

  return (
    <div>
      {lang === "RU" ? (
        <>
          <h1>Управление клиентами</h1>
          <MyModal visible={modal} setVisible={setModal}>
            <CustomersForm></CustomersForm>
          </MyModal>
          <GreenButton onClick={() => setModal(true)}>
            Добавить клиента
          </GreenButton>
          <CustomersList customers={customers} />
        </>
      ) : (
        <>
          <h1>Customers management</h1>
          <MyModal visible={modal} setVisible={setModal}>
            <CustomersForm></CustomersForm>
          </MyModal>
          <GreenButton onClick={() => setModal(true)}>Add customer</GreenButton>
          <CustomersList customers={customers} />
        </>
      )}
    </div>
  );
};

export default Customers;
