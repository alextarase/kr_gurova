// @ts-nocheck
import { observer } from "mobx-react";
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import AuthService from "../API/AuthService";
import MyInput from "../components/UI/input/MyInput";
import { AuthContext } from "../context";
import GreenButton from "./../components/UI/button/GreenButton";

const Register = () => {
  const { lang } = useContext(AuthContext);

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const registration = async () => {
    try {
      const response = await AuthService.registration(
        username,
        email,
        password
      );
      console.log("registration response in store", response);
      alert("Succeed");

      // localStorage.setItem("token", response.data.accessToken);
      // setIsAuth(true);
    } catch (e) {
      if (e.response.status === 409) {
        alert("Username or email alredy exsists");
      }
    }
  };

  return (
    <div>
      {lang === "RU" ? (
        <>
          <h1>Зарегистрируйтесь</h1>
          <form
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            <MyInput
              onChange={(e) => setUsername(e.target.value)}
              value={username}
              type="username"
              placeholder="Введите имя нового пользователя"
            />
            <MyInput
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              type="email"
              placeholder="Введите email"
              autoComplete="email"
            />
            <MyInput
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              type="password"
              placeholder="Введите пароль"
            />
            <GreenButton onClick={() => registration()}>
              Зарегистрироваться
            </GreenButton>

            <GreenButton onClick={() => navigate(`/auth`)}>
              Перейти на страницу входа
            </GreenButton>
          </form>
        </>
      ) : (
        <>
          <h1>Sign up</h1>
          <form
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            <MyInput
              onChange={(e) => setUsername(e.target.value)}
              value={username}
              type="username"
              placeholder="User name"
            />
            <MyInput
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              type="email"
              placeholder="Email"
              autoComplete="email"
            />
            <MyInput
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              type="password"
              placeholder="Password"
            />
            <GreenButton onClick={() => registration()}>Sign up</GreenButton>

            <GreenButton onClick={() => navigate(`/auth`)}>
              Go to sign in
            </GreenButton>
          </form>
        </>
      )}
    </div>
  );
};

export default observer(Register);
