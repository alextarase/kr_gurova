// @ts-nocheck
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import AuthService from "../API/AuthService";
import GreenButton from "../components/UI/button/GreenButton";
import MyInput from "../components/UI/input/MyInput";
import { AuthContext } from "../context";

const Auth = () => {
  const { isAuth, setIsAuth } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const navigate = useNavigate();

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const login = async () => {
    try {
      const response = await AuthService.login(username, email, password);
      console.log("login response in store", response);
      localStorage.setItem("token", response.data.accessToken);
      setIsAuth(true);
    } catch (e) {
      alert(e.response?.data?.message);
    }
  };

  return (
    <div>
      {lang === "RU" ? (
        <>
          <h1>Войдите в аккаунт</h1>
          <form
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            <MyInput
              onChange={(e) => setUsername(e.target.value)}
              value={username}
              type="username"
              placeholder="Введите имя пользователя"
            />
            <MyInput
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              type="email"
              placeholder="Введите email"
              autoComplete="email"
            />
            <MyInput
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              type="password"
              placeholder="Введите пароль"
            />
            <GreenButton onClick={() => navigate(`/register`)}>
              Перейти на страницу регистрации
            </GreenButton>

            <GreenButton onClick={() => login()}>Войти</GreenButton>
          </form>
        </>
      ) : (
        <>
          <h1>Sign in</h1>
          <form
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            <MyInput
              onChange={(e) => setUsername(e.target.value)}
              value={username}
              type="username"
              placeholder="User name"
            />
            <MyInput
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              type="email"
              placeholder="Email"
              autoComplete="email"
            />
            <MyInput
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              type="password"
              placeholder="Password"
            />
            <GreenButton onClick={() => navigate(`/register`)}>
              Go to sign up
            </GreenButton>

            <GreenButton onClick={() => login()}>Sign in</GreenButton>
          </form>
        </>
      )}
    </div>
  );
};

export default Auth;
