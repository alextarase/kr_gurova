import React, { useContext, useEffect, useState } from "react";
import MenuService from "../API/MenuService";
import GroupMenuForm from "../components/MenuCompponents/GroupMenuForm";
import GroupMenuList from "../components/MenuCompponents/GroupMenuList";
import GreenButton from "../components/UI/button/GreenButton";
import MyModal from "../components/UI/MyModal/MyModal";
import { AuthContext } from "../context";

const Menu = () => {
  const { modal, setModal } = useContext(AuthContext);
  const { fetching, setFetching } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const [groupMenu, setGroupMenu] = useState([]);

  async function fetchGroupMenu() {
    const response = await MenuService.getAllGroupsMenu();
    setGroupMenu(response.data);
  }

  // Отрисовывает меню при загрузке страницы
  useEffect(() => {
    fetchGroupMenu();
  }, []);

  // Получаем меню при изменении меню
  useEffect(() => {
    // getDishes();
    fetchGroupMenu();
    setFetching(false);
  }, [fetching, setFetching]);

  return (
    <div>
      {lang === "RU" ? (
        <>
          <h1>Управление меню</h1>
          <MyModal visible={modal} setVisible={setModal}>
            <GroupMenuForm></GroupMenuForm>
          </MyModal>

          <GreenButton onClick={() => setModal(true)}>
            Добавить группу блюд
          </GreenButton>
          <GroupMenuList groupMenu={groupMenu} />
        </>
      ) : (
        <>
          <h1>Menu management</h1>
          <MyModal visible={modal} setVisible={setModal}>
            <GroupMenuForm></GroupMenuForm>
          </MyModal>

          <GreenButton onClick={() => setModal(true)}>
            Add group menu Item
          </GreenButton>
          <GroupMenuList groupMenu={groupMenu} />
        </>
      )}
    </div>
  );
};

export default Menu;

// async function fetchMenu() {
//   const response = await MenuService.getAll();
//   setMenu(response.data);
//   console.log(response.data);
// }

// async function getDishes(key) {
//   const response = await MenuService.getAllDishesByGroups(props.groupMenu.id);
//   setMenu(response.data);
//   console.log(response.data, props.groupMenu.id);
// }
