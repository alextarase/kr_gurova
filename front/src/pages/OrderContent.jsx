import React, { useContext, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import OrderService from "../API/OrderService";
import OrderContentForm from "../components/OrderContentComponents/OrderContentForm";
import OrderContentList from "../components/OrderContentComponents/OrderContentList";
import GreenButton from "../components/UI/button/GreenButton";
import MyModal from "../components/UI/MyModal/MyModal";
import { AuthContext } from "../context";

const OrderContent = () => {
  const [orderContent, setOrderContent] = useState([]);
  const { modal, setModal } = useContext(AuthContext);
  const { modal2, setModal2 } = useContext(AuthContext);
  const { fetching, setFetching } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const navigate = useNavigate();

  // async function fetchOrderContent() {
  //   const response = await OrderService.getAllOrderContent();
  //   setOrderContent(response.data);
  // }

  const test = useLocation();
  const id = test.pathname.split("/")[test.pathname.split("/").length - 1];
  // console.log(test, id);

  // async function getDishes() {
  //   const response = await MenuService.getAllDishesByGroups(id);
  //   setMenu(response.data);
  //   console.log(response.data);
  // }

  async function getAllOrderContentbyOrderID() {
    const response = await OrderService.getAllOrderContentbyOrderID(id);
    setOrderContent(response.data);
  }

  // Отрисовывает заказы при загрузке страницы
  useEffect(() => {
    getAllOrderContentbyOrderID();
    // fetchOrderContent();
  }, []);

  // Получаем заказы при добавлении или удалении заказа
  useEffect(() => {
    getAllOrderContentbyOrderID();
    setFetching(false);
  }, [fetching, setFetching]);

  return (
    <div>
      {lang === "RU" ? (
        <>
          <h1>Данные заказа</h1>
          <MyModal visible={modal2} setVisible={setModal2}>
            <OrderContentForm></OrderContentForm>
          </MyModal>
          <GreenButton onClick={() => navigate(`/order`)}>
            Показать заказы
          </GreenButton>
          <GreenButton onClick={() => setModal2(true)}>
            Добавить данные заказа
          </GreenButton>
          <OrderContentList orderContent={orderContent} />
        </>
      ) : (
        <>
          <h1>Order content</h1>
          <MyModal visible={modal2} setVisible={setModal2}>
            <OrderContentForm></OrderContentForm>
          </MyModal>
          <GreenButton onClick={() => navigate(`/order`)}>
            Show orders
          </GreenButton>{" "}
          <GreenButton onClick={() => setModal2(true)}>
            Add order content
          </GreenButton>
          <OrderContentList orderContent={orderContent} />
        </>
      )}
    </div>
  );
};

export default OrderContent;
