import React, { useContext, useEffect, useState } from "react";
import BoardList from "./../components/BoardComponents/BoardList";
import BoardsService from "./../API/BoardsService";
import { AuthContext } from "../context";
import GreenButton from "../components/UI/button/GreenButton";
import BoardForm from "../components/BoardComponents/BoardForm";
import MyModal from "../components/UI/MyModal/MyModal";
import { useNavigate } from "react-router-dom";

const Boards = () => {
  const [board, setBoard] = useState([]);
  const { modal, setModal } = useContext(AuthContext);
  const { fetching, setFetching } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);

  const navigate = useNavigate();

  async function fetchBoards() {
    const response = await BoardsService.getAll();
    setBoard(response.data);
  }

  // Отрисовывает столы при загрузке страницы
  useEffect(() => {
    fetchBoards();
  }, []);

  // Получаем столы при добавлении или удалении столов
  useEffect(() => {
    fetchBoards();
    setFetching(false);
  }, [fetching, setFetching]);

  return (
    <div>
      {lang === "RU" ? (
        <>
          <h1>Управление столиками</h1>
          <MyModal visible={modal} setVisible={setModal}>
            <BoardForm></BoardForm>
          </MyModal>
          <GreenButton onClick={() => setModal(true)}>
            Добавить столик
          </GreenButton>

          <BoardList board={board} />
        </>
      ) : (
        <>
          <h1>Boards Management</h1>
          <MyModal visible={modal} setVisible={setModal}>
            <BoardForm></BoardForm>
          </MyModal>
          <GreenButton onClick={() => setModal(true)}>Add board</GreenButton>
          <BoardList board={board} />
        </>
      )}
    </div>
  );
};

export default Boards;
