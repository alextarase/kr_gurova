import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import OrderService from "../API/OrderService";
import OrderForm from "../components/OrderComponents/OrderForm";
import OrderList from "../components/OrderComponents/OrderList";
import GreenButton from "../components/UI/button/GreenButton";
import MyModal from "../components/UI/MyModal/MyModal";
import { AuthContext } from "../context";

const Order = () => {
  const [order, setOrder] = useState([]);
  const { modal, setModal } = useContext(AuthContext);
  const { fetching, setFetching } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const navigate = useNavigate();

  async function fetchOrder() {
    const response = await OrderService.getAll();
    setOrder(response.data);
  }

  // Отрисовывает заказы при загрузке страницы
  useEffect(() => {
    fetchOrder();
  }, []);

  // Получаем заказы при добавлении или удалении меню
  useEffect(() => {
    fetchOrder();
    setFetching(false);
  }, [fetching, setFetching]);

  return (
    <div>
      {lang === "RU" ? (
        <>
          <h1>Управление заказами</h1>
          <MyModal visible={modal} setVisible={setModal}>
            <OrderForm></OrderForm>
          </MyModal>
          <GreenButton onClick={() => setModal(true)}>
            Добавить заказ
          </GreenButton>

          <OrderList order={order} />
        </>
      ) : (
        <>
          <h1>Order management</h1>
          <MyModal visible={modal} setVisible={setModal}>
            <OrderForm></OrderForm>
          </MyModal>
          <GreenButton onClick={() => setModal(true)}>Add order</GreenButton>
          <GreenButton onClick={() => navigate(`/order_content`)}>
            Show order's content
          </GreenButton>
          <OrderList order={order} />
        </>
      )}
    </div>
  );
};

export default Order;
