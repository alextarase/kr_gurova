import React, { useContext, useEffect, useState } from "react";
import StaffList from "../components/StaffComponents/StaffList";
import GreenButton from "../components/UI/button/GreenButton";
import StaffService from "../API/StaffService";
import StaffForm from "../components/StaffComponents/StaffForm";
import { AuthContext } from "./../context/index";
import MyModal from "../components/UI/MyModal/MyModal";
import MySelect from "./../components/UI/Select/MySelect";

const Staff = () => {
  const [staff, setStaff] = useState([]);
  const { modal, setModal } = useContext(AuthContext);
  const { fetching, setFetching } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const [selectedSort, setSelectedSort] = useState("");

  async function fetchStaff() {
    const response = await StaffService.getAll();
    setStaff(response.data);
    console.log("staff log", response.data);
  }

  // Отрисовывает персонал при загрузке страницы
  useEffect(() => {
    fetchStaff();
  }, []);

  // Получаем персонал при добавлении или удалении персонала
  useEffect(() => {
    fetchStaff();
    setFetching(false);
  }, [fetching, setFetching]);

  const sortStaff = (sort) => {
    setSelectedSort(sort);
    setStaff([...staff].sort((a, b) => a[sort].localeCompare(b[sort])));
  };

  return (
    <div>
      {lang === "RU" ? (
        <>
          <h1>Управление пероналом</h1>
          <MyModal visible={modal} setVisible={setModal}>
            <StaffForm></StaffForm>
          </MyModal>
          <div>
            <MySelect
              value={selectedSort}
              onChange={sortStaff}
              defaultValue="Сортировка по"
              options={[
                { value: "position", name: "По должности" },
                // { value: "id", name: "По Id" },
                { value: "name", name: "По имени" },
              ]}
            />
          </div>
          <GreenButton onClick={() => setModal(true)}>
            Добавить работника
          </GreenButton>
          <StaffList staff={staff} />
        </>
      ) : (
        <>
          <h1>Staff Management</h1>
          <MyModal visible={modal} setVisible={setModal}>
            <StaffForm></StaffForm>
          </MyModal>
          <div>
            <MySelect
              value={selectedSort}
              onChange={sortStaff}
              defaultValue="Sort by"
              options={[
                { value: "position", name: "position" },
                // { value: "id", name: "По Id" },
                { value: "name", name: "name" },
              ]}
            />
          </div>
          <GreenButton onClick={() => setModal(true)}>Add staff</GreenButton>
          <StaffList staff={staff} />
        </>
      )}
    </div>
  );
};

export default Staff;
