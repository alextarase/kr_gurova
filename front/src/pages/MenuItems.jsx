import React, { useContext, useEffect, useState } from "react";
import { useLocation, useNavigate, useRoutes } from "react-router-dom";
import MenuService from "../API/MenuService";
import MenuForm from "../components/MenuCompponents/MenuForm";
import MenuList from "../components/MenuCompponents/MenuList";
import GreenButton from "../components/UI/button/GreenButton";
import MyModal from "../components/UI/MyModal/MyModal";
import { AuthContext } from "../context";

const MenuItems = ({ ...props }) => {
  const { modal, setModal } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const [menu, setMenu] = useState([]);
  const { fetching, setFetching } = useContext(AuthContext);
  const navigate = useNavigate();

  const test = useLocation();
  const id = test.pathname.split("/")[test.pathname.split("/").length - 1];

  async function getDishes() {
    const response = await MenuService.getAllDishesByGroups(id);
    setMenu(response.data);
    console.log(response.data);
  }

  // Отрисовывает меню при загрузке страницы
  useEffect(() => {
    getDishes();
  }, []);

  // Получаем меню при изменении меню
  useEffect(() => {
    getDishes();
    setFetching(false);
  }, [fetching, setFetching]);

  // console.log(test.pathname.substring(12));
  console.log(test.pathname.split("/")[test.pathname.split("/").length - 1]);

  return (
    <div>
      {lang === "RU" ? (
        <>
          <MyModal visible={modal} setVisible={setModal}>
            <MenuForm></MenuForm>
          </MyModal>
          <GreenButton onClick={() => setModal(true)}>
            Добавить блюдо
          </GreenButton>
          <GreenButton onClick={() => navigate(`/menu`)}>Назад</GreenButton>
          <MenuList menu={menu} />
        </>
      ) : (
        <>
          <MyModal visible={modal} setVisible={setModal}>
            <MenuForm></MenuForm>
          </MyModal>
          <GreenButton onClick={() => setModal(true)}>Add dish</GreenButton>
          <GreenButton onClick={() => navigate(`/menu`)}>Exit</GreenButton>
          <MenuList menu={menu} />
        </>
      )}
    </div>
  );
};

export default MenuItems;
