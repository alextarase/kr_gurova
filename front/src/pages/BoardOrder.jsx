import React, { useContext, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import BoardsService from "../API/BoardsService";
import BoardOrderList from "../components/BoardOrderComponents/BoardOrderList";
import GreenButton from "../components/UI/button/GreenButton";
import { AuthContext } from "../context";
import MyModal from "./../components/UI/MyModal/MyModal";
import BoardOrderForm from "./../components/BoardOrderComponents/BoardOrderForm";

const BoardOrder = () => {
  const [boardOrder, setBoardOrder] = useState([]);
  const { fetching, setFetching } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const { modal, setModal } = useContext(AuthContext);

  const navigate = useNavigate();
  const test = useLocation();
  const id = test.pathname.split("/")[test.pathname.split("/").length - 1];
  console.log("id in board order", id);

  //   const { modal, setModal } = useContext(AuthContext);
  //   const { fetching, setFetching } = useContext(AuthContext);

  async function fetchBoards() {
    const response = await BoardsService.getAllOrdersByboard(id);
    setBoardOrder(response.data);
    console.log(response.data);
  }

  // Отрисовывает заказы столов при загрузке страницы
  useEffect(() => {
    fetchBoards();
  }, []);

  useEffect(() => {
    fetchBoards();
  }, [fetching, setFetching]);

  return (
    <div>
      {lang === "RU" ? (
        <>
          <h1>Управление заказами столиков</h1>{" "}
          <MyModal visible={modal} setVisible={setModal}>
            <BoardOrderForm></BoardOrderForm>
          </MyModal>
          <GreenButton onClick={() => navigate(`/boards`)}>
            Показать столики
          </GreenButton>{" "}
          <GreenButton onClick={() => setModal(true)}>
            Создать заказ столика
          </GreenButton>
          <BoardOrderList boardOrder={boardOrder} />
        </>
      ) : (
        <>
          <h1>Board order management</h1>{" "}
          <MyModal visible={modal} setVisible={setModal}>
            <BoardOrderForm></BoardOrderForm>
          </MyModal>
          <GreenButton onClick={() => navigate(`/boards`)}>
            Show boards
          </GreenButton>
          <GreenButton onClick={() => setModal(true)}>
            Create board order
          </GreenButton>
          <BoardOrderList boardOrder={boardOrder} />
        </>
      )}
    </div>
  );
};

export default BoardOrder;
