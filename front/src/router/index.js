import { Navigate } from "react-router-dom";
import Boards from "../pages/Boards";
import Customers from "../pages/Customers";
import Menu from "../pages/Menu";
import Staff from "../pages/Staff";
import Register from "./../pages/Register";
import BoardOrder from "./../pages/BoardOrder";
import Order from "../pages/Order";
import OrderContent from "./../pages/OrderContent";
import MenuItems from "./../pages/MenuItems";
import Auth from "../pages/Auth";

export const privateRoutes = [
  { path: "/staff", element: <Staff /> },
  { path: "/boards", element: <Boards /> },
  { path: "/customers", element: <Customers /> },
  { path: "/menu", element: <Menu /> },
  { path: "/order", element: <Order /> },
  { path: "/order_content/:id", element: <OrderContent /> },
  { path: "/menu_items/:id", element: <MenuItems /> },
  { path: "/board_order", element: <BoardOrder /> },
  { path: "/board_order/:id", element: <BoardOrder /> },
  { path: "*", element: <Navigate to="/staff" /> },
];

export const pablicRoutes = [
  { path: "/register", element: <Register /> },
  { path: "/auth", element: <Auth /> },
  { path: "*", element: <Navigate to="/auth" /> },
];
