import React from "react";
import BoardItem from "./BoardItem";

const BoardList = ({ board }) => {
  return (
    <div>
      {board.map((board) => (
        <BoardItem key={board.id} board={board} />
      ))}
    </div>
  );
};

export default BoardList;
