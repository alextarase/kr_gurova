import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyModal from "../UI/MyModal/MyModal";
import BoardService from "./../../API/BoardsService";

const BoardItem = ({ ...props }) => {
  const { setFetching } = useContext(AuthContext);
  const { modal2, setModal2 } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const navigate = useNavigate();

  const removeItem = (str) => {
    const result = window.confirm(str);
    if (result) {
      console.log("id in remove", props.board.id);
      BoardService.delete(props.board.id);
      setFetching(true);
      setModal2(false);
    }
  };

  const ChangeStatus = () => {
    if (props.board.status === "ORDERED") {
      props.board.status = "BUSY";
      BoardService.patchBoardStatus(props.board.id, props.board.status);
      setFetching(true);
    } else if (props.board.status === "BUSY") {
      props.board.status = "FREE";
      BoardService.patchBoardStatus(props.board.id, props.board.status);
      setFetching(true);
    } else if (props.board.status === "FREE") {
      props.board.status = "ORDERED";
      BoardService.patchBoardStatus(props.board.id, props.board.status);
      setFetching(true);
    }
  };

  return (
    <div className="board">
      {lang === "RU" ? (
        <>
          <div className="board__content">
            <strong>ID.{props.board.id}</strong>
            <div>
              Номер: {props.board.number}. Статус:{" "}
              <strong>{props.board.status}.</strong> Количество мест:{" "}
              {props.board.sit_number}.
            </div>
          </div>

          <GreenButton onClick={() => removeItem("Вы уверены?")}>
            Удалить столик
          </GreenButton>

          <GreenButton onClick={() => ChangeStatus()}>
            Изменить статус
          </GreenButton>
          <GreenButton
            onClick={() => navigate(`/board_order/${props.board.id}`)}
          >
            Показать заказы столика
          </GreenButton>
        </>
      ) : (
        <>
          <div className="board__content">
            <strong>Board ID.{props.board.id}</strong>
            <div>
              Number: {props.board.number}. Status:{" "}
              <strong>{props.board.status}.</strong> Sit number:{" "}
              {props.board.sit_number}.
            </div>
          </div>

          <GreenButton onClick={() => removeItem("Are you sure?")}>
            Delete board
          </GreenButton>

          <GreenButton onClick={() => ChangeStatus()}>
            Change status
          </GreenButton>
          <GreenButton
            onClick={() => navigate(`/board_order/${props.board.id}`)}
          >
            Show board orders
          </GreenButton>
        </>
      )}
    </div>
  );
};

export default BoardItem;
