import React, { useContext, useState } from "react";
import BoardService from "../../API/BoardsService";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyInput from "../UI/input/MyInput";

const BoardForm = () => {
  const [boardNumber, setBoardNumber] = useState("");
  const [boardSitNumber, setBoardSitNumber] = useState("");
  const { fetching, setFetching } = useContext(AuthContext);
  const { modal, setModal } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);

  async function addNewBoard(e) {
    e.preventDefault();
    const response = await BoardService.add(boardNumber, boardSitNumber);
    setFetching(true);
    console.log(response);
    setBoardNumber("");
    setBoardSitNumber("");
    setModal();
  }

  return (
    <form>
      {lang === "RU" ? (
        <>
          <MyInput
            onChange={(e) => setBoardNumber(e.target.value)}
            value={boardNumber}
            type="text"
            placeholder="Номер"
          />
          <MyInput
            onChange={(e) => setBoardSitNumber(e.target.value)}
            value={boardSitNumber}
            type="text"
            placeholder="Количество мест"
          />
          <GreenButton onClick={addNewBoard}>Создать столик</GreenButton>
        </>
      ) : (
        <>
          <MyInput
            onChange={(e) => setBoardNumber(e.target.value)}
            value={boardNumber}
            type="text"
            placeholder="Number"
          />
          <MyInput
            onChange={(e) => setBoardSitNumber(e.target.value)}
            value={boardSitNumber}
            type="text"
            placeholder="Sit number"
          />
          <GreenButton onClick={addNewBoard}>Create board</GreenButton>
        </>
      )}
    </form>
  );
};

export default BoardForm;
