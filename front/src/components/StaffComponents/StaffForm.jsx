import React, { useContext, useEffect, useState } from "react";
import StaffService from "../../API/StaffService";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyInput from "../UI/input/MyInput";

const StaffForm = () => {
  const [staffName, setstaffName] = useState("");
  const [staffSalary, setstaffSalary] = useState("");
  const [staffPhonenumber, setstaffPhonenumber] = useState("");
  const { fetching, setFetching } = useContext(AuthContext);
  const { modal, setModal } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);

  async function addNewStaff(e) {
    e.preventDefault();
    const response = await StaffService.add(
      staffName,
      staffSalary,
      staffPhonenumber
    );
    setFetching(true);
    console.log(response);
    setstaffName("");
    setstaffSalary("");
    setstaffPhonenumber("");
    setModal();
  }

  return (
    <form
    // onSubmit={(e) => e.preventDefault()}
    >
      {lang === "RU" ? (
        <>
          <MyInput
            onChange={(e) => setstaffName(e.target.value)}
            value={staffName}
            type="text"
            placeholder="Имя работника"
          />
          <MyInput
            onChange={(e) => setstaffSalary(e.target.value)}
            value={staffSalary}
            type="text"
            placeholder="Зарплата работника"
          />
          <MyInput
            onChange={(e) => setstaffPhonenumber(e.target.value)}
            value={staffPhonenumber}
            type="text"
            placeholder="Номер телефона работника"
          />
          <GreenButton onClick={addNewStaff}>Добавить работника</GreenButton>
        </>
      ) : (
        <>
          <MyInput
            onChange={(e) => setstaffName(e.target.value)}
            value={staffName}
            type="text"
            placeholder="Staff name"
          />
          <MyInput
            onChange={(e) => setstaffSalary(e.target.value)}
            value={staffSalary}
            type="text"
            placeholder="Staff salary"
          />
          <MyInput
            onChange={(e) => setstaffPhonenumber(e.target.value)}
            value={staffPhonenumber}
            type="text"
            placeholder="Staff phone number"
          />
          <GreenButton onClick={addNewStaff}>Create staff</GreenButton>
        </>
      )}
    </form>
  );
};

export default StaffForm;
