import React, { useContext, useEffect } from "react";
import GreenButton from "../UI/button/GreenButton";
import StaffService from "../../API/StaffService";
import { AuthContext } from "../../context";
import MyModal from "../UI/MyModal/MyModal";
import { ErrorBoundary } from "react-error-boundary";
import ErrorFallback from "../ErrorFallback/Error.jsx";

const StaffItem = ({ ...props }) => {
  const { fetching, setFetching } = useContext(AuthContext);
  const { lang, setLang } = useContext(AuthContext);
  const { modal2, setModal2 } = useContext(AuthContext);

  const removeItem = (str) => {
    const result = window.confirm(str);
    if (result) {
      StaffService.delete(props.staff.id);
    }
    setFetching(true);
    setModal2(false);
  };

  return (
    <div className="staff">
      <div className="staff__content">
        <strong>
          {props.staff.id}. {props.staff.name}
        </strong>
        {lang === "RU" ? (
          <>
            <div>
              Номер телефона: {props.staff.phone_number}
              .Должность: {props.staff.position}
            </div>
          </>
        ) : (
          <>
            <div>
              Phone namber: {props.staff.phone_number}
              .Position: {props.staff.position}
            </div>
          </>
        )}
      </div>
      {lang === "RU" ? (
        <>
          <GreenButton onClick={() => removeItem("Вы уверены?")}>
            Удалить
          </GreenButton>
        </>
      ) : (
        <>
          <GreenButton onClick={() => removeItem("Are you sure?")}>
            Delete
          </GreenButton>
        </>
      )}
    </div>
  );
};

export default StaffItem;

/* <GreenButton>Edit position</GreenButton>
      <select name="position">
        <option value="Waiter">Waiter</option>
        <option value="Manager">Manager</option>
        <option value="Admin">Admin</option>
        <option value="Boss">Boss</option>
      </select> */
