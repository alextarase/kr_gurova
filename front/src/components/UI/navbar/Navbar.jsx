import { observer } from "mobx-react";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { Context } from "../../..";
import { AuthContext } from "../../../context";
import GreenButton from "./../button/GreenButton";

const Navbar = () => {
  const { setIsAuth } = useContext(AuthContext);
  const { lang, setLang } = useContext(AuthContext);

  const logout = async () => {
    try {
      localStorage.removeItem("token");
      setIsAuth(false);
    } catch (e) {
      console.log(e.response?.data?.message);
    }
  };

  const changeLang = () => {
    if (lang === "RU") {
      setLang("EN");
    } else {
      setLang("RU");
    }
    console.log(lang);
  };

  return (
    <div className="navbar">
      {lang === "RU" ? (
        <>
          <GreenButton style={{ marginLeft: 20 }} onClick={() => logout()}>
            Выход
          </GreenButton>
          <GreenButton style={{ marginLeft: 20 }} onClick={() => changeLang()}>
            RU
          </GreenButton>
          <div className="navbar__links">
            <Link to="/staff">Персонал</Link>
            <Link to="/boards">Столики</Link>
            <Link to="/customers">Клиенты</Link>
            <Link to="/menu">Меню</Link>
            <Link to="/order">Заказ </Link>

            {/* <Link to="/register">Registration</Link> */}
          </div>
        </>
      ) : (
        <>
          <GreenButton style={{ marginLeft: 20 }} onClick={() => logout()}>
            Log out
          </GreenButton>
          <GreenButton style={{ marginLeft: 20 }} onClick={() => changeLang()}>
            EN
          </GreenButton>
          <div className="navbar__links">
            <Link to="/staff">Staff</Link>
            <Link to="/boards">Boards</Link>
            <Link to="/customers">Customers</Link>
            <Link to="/menu">Menu</Link>
            <Link to="/order">Order </Link>

            {/* <Link to="/register">Registration</Link> */}
          </div>
        </>
      )}
    </div>
  );
};

export default observer(Navbar);
