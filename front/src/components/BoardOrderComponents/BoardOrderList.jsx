import React from "react";
import BoardOrderItem from "./BoardOrderItem";

const BoardOrderList = ({ boardOrder }) => {
  return (
    <div>
      {boardOrder.map((boardOrder) => (
        <BoardOrderItem key={boardOrder.id} boardOrder={boardOrder} />
      ))}
    </div>
  );
};

export default BoardOrderList;
