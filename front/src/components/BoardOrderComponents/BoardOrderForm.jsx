import React, { useContext, useState } from "react";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyInput from "../UI/input/MyInput";
import BoardService from "./../../API/BoardsService";

const BoardOrderForm = () => {
  const { fetching, setFetching } = useContext(AuthContext);
  const { modal, setModal } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const [boardOrder, setBoardOrder] = useState({
    Date_time_ordered: "",
    name: "",
    phone_number: "",
    sit_number: "",
    boardID: "",
  });

  async function addNewBoardOrder(e) {
    e.preventDefault();

    const response = await BoardService.addBoardOrder(
      boardOrder.Date_time_ordered,
      boardOrder.name,
      boardOrder.phone_number,
      boardOrder.sit_number,
      boardOrder.boardID
    );
    setFetching(true);
    setModal();
    console.log(response);
  }

  return (
    <form>
      {lang === "RU" ? (
        <>
          <MyInput
            onChange={(e) =>
              setBoardOrder({
                ...boardOrder,
                Date_time_ordered: e.target.value,
              })
            }
            value={boardOrder.Date_time_ordered}
            type="text"
            placeholder="Дата и время заказа"
          />{" "}
          <MyInput
            onChange={(e) =>
              setBoardOrder({
                ...boardOrder,
                name: e.target.value,
              })
            }
            value={boardOrder.name}
            type="text"
            placeholder="Имя заказчика"
          />{" "}
          <MyInput
            onChange={(e) =>
              setBoardOrder({
                ...boardOrder,
                phone_number: e.target.value,
              })
            }
            value={boardOrder.phone_number}
            type="text"
            placeholder="Номер телефона"
          />{" "}
          <MyInput
            onChange={(e) =>
              setBoardOrder({
                ...boardOrder,
                sit_number: e.target.value,
              })
            }
            value={boardOrder.sit_number}
            type="number"
            placeholder="Количество мест"
          />{" "}
          <MyInput
            onChange={(e) =>
              setBoardOrder({
                ...boardOrder,
                boardID: e.target.value,
              })
            }
            value={boardOrder.boardID}
            type="number"
            placeholder="Id столика"
          />
          <GreenButton onClick={addNewBoardOrder}>
            Создать заказ стлика
          </GreenButton>
        </>
      ) : (
        <>
          <MyInput
            onChange={(e) =>
              setBoardOrder({
                ...boardOrder,
                Date_time_ordered: e.target.value,
              })
            }
            value={boardOrder.Date_time_ordered}
            type="number"
            placeholder="Date time ordered"
          />{" "}
          <MyInput
            onChange={(e) =>
              setBoardOrder({
                ...boardOrder,
                name: e.target.value,
              })
            }
            value={boardOrder.name}
            type="text"
            placeholder="Name"
          />{" "}
          <MyInput
            onChange={(e) =>
              setBoardOrder({
                ...boardOrder,
                phone_number: e.target.value,
              })
            }
            value={boardOrder.phone_number}
            type="text"
            placeholder="phone number"
          />{" "}
          <MyInput
            onChange={(e) =>
              setBoardOrder({
                ...boardOrder,
                sit_number: e.target.value,
              })
            }
            value={boardOrder.sit_number}
            type="number"
            placeholder="sit number"
          />{" "}
          <MyInput
            onChange={(e) =>
              setBoardOrder({
                ...boardOrder,
                boardID: e.target.value,
              })
            }
            value={boardOrder.boardID}
            type="number"
            placeholder="board ID"
          />
          <GreenButton onClick={addNewBoardOrder}>
            Create board order
          </GreenButton>
        </>
      )}
    </form>
  );
};

export default BoardOrderForm;
