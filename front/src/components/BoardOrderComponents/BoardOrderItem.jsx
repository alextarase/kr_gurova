import React, { useContext } from "react";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";

const BoardOrderItem = ({ ...props }) => {
  const { fetching, setFetching } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);

  //   const removeItem = (id) => {
  //     BoardService.delete(props.boardOrder.id);
  //     setFetching(true);
  //   };

  return (
    <div className="boardOrder">
      {lang === "RU" ? (
        <div className="boardOrder__content">
          <div>
            <strong>ID: {props.boardOrder.id}.</strong> Имя:{" "}
            {props.boardOrder.name}.{" "}
            <strong>Номер телефона: {props.boardOrder.phone_number}.</strong>{" "}
            Количесво мест: {props.boardOrder.sit_number}. Дата и время
            бронирования {props.boardOrder.Date_time_ordered.slice(0, -14)}.{" "}
            {props.boardOrder.Date_time_ordered.slice(11, -5)}.
          </div>
        </div>
      ) : (
        <div className="boardOrder__content">
          <div>
            <strong>BoardID: {props.boardOrder.id}.</strong> Name:{" "}
            {props.boardOrder.name}.{" "}
            <strong>phone_number: {props.boardOrder.phone_number}.</strong> Sit
            number: {props.boardOrder.sit_number}. Date_time_ordered{" "}
            {props.boardOrder.Date_time_ordered.slice(0, -14)}.{" "}
            {props.boardOrder.Date_time_ordered.slice(11, -5)}.
          </div>
        </div>
      )}
    </div>
  );
};

export default BoardOrderItem;

/* <GreenButton
      //   onClick={() => removeItem(props.boardOrder.id)}
      >
        Finish board order
      </GreenButton> */
