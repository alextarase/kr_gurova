import React from "react";
import OrderItem from "./OrderItem";

const OrderList = ({ order }) => {
  return (
    <div>
      {order.map((order) => (
        <OrderItem key={order.id} order={order} />
      ))}
    </div>
  );
};

export default OrderList;
