import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import OrderService from "../../API/OrderService";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyModal from "../UI/MyModal/MyModal";

const OrderItem = ({ ...props }) => {
  const { setFetching } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const { modal2, setModal2 } = useContext(AuthContext);
  const navigate = useNavigate();

  const removeItem = (str) => {
    const result = window.confirm(str);
    if (result) {
      OrderService.delete(props.order.id);
      setFetching(true);
      setModal2(false);
    }
  };

  const getOrderContent = (id) => {
    navigate(`/order_content/${id}`);
  };

  const ChangeStatus = () => {
    if (props.order.status === "IN_PROGRESS") {
      props.order.status = "DONE";
      OrderService.patchOrderStatus(props.order.id, props.order.status);
      setFetching(true);
    } else if (props.order.status === "DONE") {
      props.order.status = "OPEN";
      OrderService.patchOrderStatus(props.order.id, props.order.status);
      setFetching(true);
    } else if (props.order.status === "OPEN") {
      props.order.status = "IN_PROGRESS";
      OrderService.patchOrderStatus(props.order.id, props.order.status);
      setFetching(true);
    }
  };
  return (
    <div className="order">
      <div className="order__content">
        {lang === "RU" ? (
          <>
            <strong>ID: {props.order.id}.</strong>
            <div>
              №: <strong>{props.order.number}.</strong> Имя:{" "}
              <strong>{props.order.name}</strong>. Оплаченая сумма:{" "}
              <strong>{props.order.payed_summ}.</strong> Статус:
              <strong> {props.order.status}.</strong> Дата и время открытия
              заказа:
              <strong>{props.order.Date_time_start.slice(0, -14)}</strong>.{" "}
              <strong>{props.order.Date_time_start.slice(11, -5)}</strong>.
              <br /> Дата и время закрытия заказа:{" "}
              <strong>
                {props.order.Date_time_finish.slice(0, -14)}.
              </strong>{" "}
              <strong>{props.order.Date_time_finish.slice(11, -5)}.</strong>
            </div>
          </>
        ) : (
          <>
            <strong>ID: {props.order.id}.</strong>
            <div>
              №: <strong>{props.order.number}.</strong> Name:{" "}
              <strong>{props.order.name}</strong>. Payed summ:{" "}
              <strong>{props.order.payed_summ}.</strong> Status:
              <strong> {props.order.status}.</strong> Date_time_start:{" "}
              <strong>{props.order.Date_time_start.slice(0, -14)}</strong>.{" "}
              <strong>{props.order.Date_time_start.slice(11, -5)}</strong>.
              Date_time_finish:{" "}
              <strong>{props.order.Date_time_finish.slice(0, -14)}.</strong>{" "}
              <strong>{props.order.Date_time_finish.slice(11, -5)}.</strong>
            </div>
          </>
        )}
      </div>
      {lang === "RU" ? (
        <>
          <GreenButton onClick={() => removeItem("Вы уверены?")}>
            Удалить заказ
          </GreenButton>
          <GreenButton onClick={() => ChangeStatus()}>
            Изменить статус
          </GreenButton>
          <GreenButton onClick={() => getOrderContent(props.order.id)}>
            Данные заказа
          </GreenButton>
        </>
      ) : (
        <>
          <GreenButton onClick={() => removeItem("Are you sure?")}>
            Delete order
          </GreenButton>
          <GreenButton onClick={() => ChangeStatus()}>Patch order</GreenButton>
          <GreenButton onClick={() => getOrderContent(props.order.id)}>
            Order content
          </GreenButton>
        </>
      )}
    </div>
  );
};

export default OrderItem;
