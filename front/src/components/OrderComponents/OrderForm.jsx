import React, { useContext, useState } from "react";
import OrderService from "../../API/OrderService";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyInput from "../UI/input/MyInput";

const OrderForm = () => {
  const { setFetching } = useContext(AuthContext);
  const { setModal } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);

  const [order, setOrder] = useState({
    name: "",
    number: "",
    payed_summ: "",
    Date_time_start: "",
    Date_time_finish: "",
    staffID: "",
    boardID: "",
    customersID: "",
  });

  async function addNewOrder(e) {
    e.preventDefault();

    const response = await OrderService.add(
      order.name,
      order.number,
      order.payed_summ,
      order.Date_time_start,
      order.Date_time_finish,
      order.staffID,
      order.boardID,
      order.customersID
    );
    console.log(response);
    setFetching(true);
    setModal();
    // setOrder({
    //   name: "",
    //   number: "",
    //   payed_summ: "",
    //   Date_time_start: "",
    //   Date_time_finish: "",
    //   staffID: "",
    //   boardID: "",
    //   customersID: "",
    // });
  }
  return (
    <form>
      {lang === "RU" ? (
        <>
          <MyInput
            onChange={(e) => setOrder({ ...order, name: e.target.value })}
            value={order.name}
            type="text"
            placeholder="Имя заказа"
          />
          <MyInput
            onChange={(e) => setOrder({ ...order, number: e.target.value })}
            value={order.number}
            type="text"
            placeholder="Номер заказа"
          />
          <MyInput
            onChange={(e) => setOrder({ ...order, payed_summ: e.target.value })}
            value={order.payed_summ}
            type="text"
            placeholder="Оплаченая сумма"
          />
          <MyInput
            onChange={(e) =>
              setOrder({ ...order, Date_time_start: e.target.value })
            }
            value={order.Date_time_start}
            type="text"
            placeholder="Дата и время заказа"
          />
          <MyInput
            onChange={(e) =>
              setOrder({ ...order, Date_time_finish: e.target.value })
            }
            value={order.Date_time_finish}
            type="text"
            placeholder="Дата и время окончания заказа"
          />
          <MyInput
            onChange={(e) => setOrder({ ...order, staffID: e.target.value })}
            value={order.staffID}
            type="text"
            placeholder="Идентификатор сотрудника UNIQ"
          />
          <MyInput
            onChange={(e) => setOrder({ ...order, boardID: e.target.value })}
            value={order.boardID}
            type="text"
            placeholder="Идентификатор стола UNIQ"
          />
          <MyInput
            onChange={(e) =>
              setOrder({ ...order, customersID: e.target.value })
            }
            value={order.customersID}
            type="text"
            placeholder="Идентификатор клиента UNIQ"
          />
          <GreenButton onClick={addNewOrder}>Создать заказ</GreenButton>
        </>
      ) : (
        <>
          <MyInput
            onChange={(e) => setOrder({ ...order, name: e.target.value })}
            value={order.name}
            type="text"
            placeholder="Order name"
          />
          <MyInput
            onChange={(e) => setOrder({ ...order, number: e.target.value })}
            value={order.number}
            type="text"
            placeholder="Order number"
          />
          <MyInput
            onChange={(e) => setOrder({ ...order, payed_summ: e.target.value })}
            value={order.payed_summ}
            type="text"
            placeholder="Order payed_summ"
          />
          <MyInput
            onChange={(e) =>
              setOrder({ ...order, Date_time_start: e.target.value })
            }
            value={order.Date_time_start}
            type="text"
            placeholder="Order Date_time_start"
          />
          <MyInput
            onChange={(e) =>
              setOrder({ ...order, Date_time_finish: e.target.value })
            }
            value={order.Date_time_finish}
            type="text"
            placeholder="Order Date_time_finish"
          />
          <MyInput
            onChange={(e) => setOrder({ ...order, staffID: e.target.value })}
            value={order.staffID}
            type="text"
            placeholder="Order staffID UNIQ"
          />
          <MyInput
            onChange={(e) => setOrder({ ...order, boardID: e.target.value })}
            value={order.boardID}
            type="text"
            placeholder="Order boardID UNIQ"
          />
          <MyInput
            onChange={(e) =>
              setOrder({ ...order, customersID: e.target.value })
            }
            value={order.customersID}
            type="text"
            placeholder="Order customersID UNIQ"
          />
          <GreenButton onClick={addNewOrder}>Create order</GreenButton>
        </>
      )}
    </form>
  );
};

export default OrderForm;

//datetime
