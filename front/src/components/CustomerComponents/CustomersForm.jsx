import React, { useContext, useState } from "react";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyInput from "../UI/input/MyInput";
import CustomersService from "./../../API/CustomersService";

const CustomersForm = () => {
  const { fetching, setFetching } = useContext(AuthContext);
  const { modal, setModal } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const [customer, setCustomer] = useState({
    name: "",
    bonus_programm: "",
    phone_number: "",
    curd_number: "",
  });

  async function addNewCustomer(e) {
    e.preventDefault();

    const response = await CustomersService.add(
      customer.name,
      customer.bonus_programm,
      customer.phone_number,
      customer.curd_number
    );
    console.log(response);
    setFetching(true);
    setModal();
    // setCustomer({
    //   name: "",
    //   bonus_programm: "",
    //   phone_number: "",
    //   curd_number: "",
    // });
  }

  return (
    <form>
      {lang === "RU" ? (
        <>
          <MyInput
            onChange={(e) => setCustomer({ ...customer, name: e.target.value })}
            value={customer.name}
            type="text"
            placeholder="Имя клиента"
          />
          <MyInput
            onChange={(e) =>
              setCustomer({ ...customer, bonus_programm: e.target.value })
            }
            value={customer.bonus_programm}
            type="text"
            placeholder="Бонусная программа"
          />
          <MyInput
            onChange={(e) =>
              setCustomer({ ...customer, phone_number: e.target.value })
            }
            value={customer.phone_number}
            type="text"
            placeholder="Номер телефона"
          />
          <MyInput
            onChange={(e) =>
              setCustomer({ ...customer, curd_number: e.target.value })
            }
            value={customer.curd_number}
            type="text"
            placeholder="Номер бонусной карты"
          />
          <GreenButton onClick={addNewCustomer}>Добавить клиента</GreenButton>
        </>
      ) : (
        <>
          <MyInput
            onChange={(e) => setCustomer({ ...customer, name: e.target.value })}
            value={customer.name}
            type="text"
            placeholder="Customer name"
          />
          <MyInput
            onChange={(e) =>
              setCustomer({ ...customer, bonus_programm: e.target.value })
            }
            value={customer.bonus_programm}
            type="text"
            placeholder="Customer bonus programm"
          />
          <MyInput
            onChange={(e) =>
              setCustomer({ ...customer, phone_number: e.target.value })
            }
            value={customer.phone_number}
            type="text"
            placeholder="Customer phone number"
          />
          <MyInput
            onChange={(e) =>
              setCustomer({ ...customer, curd_number: e.target.value })
            }
            value={customer.curd_number}
            type="text"
            placeholder="Customer bonus card number 800-890-850-452"
          />
          <GreenButton onClick={addNewCustomer}>Create customer</GreenButton>
        </>
      )}
    </form>
  );
};

export default CustomersForm;
