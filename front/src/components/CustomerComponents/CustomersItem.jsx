import React, { useContext } from "react";
import CustomersService from "../../API/CustomersService";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyModal from "../UI/MyModal/MyModal";

const CustomersItem = ({ customers }) => {
  const { setFetching } = useContext(AuthContext);
  const { modal2, setModal2 } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);

  const removeItem = (str) => {
    const result = window.confirm(str);
    if (result) {
      CustomersService.delete(customers.id);
      setFetching(true);
      setModal2(false);
    }
  };

  return (
    <div className="customer">
      {lang === "RU" ? (
        <>
          <div className="customer__content">
            <strong> ID клиента {customers.id}.</strong>
            <div>
              <strong>Имя: </strong> {customers.name}.{" "}
              <strong>Бонусная программа: </strong>
              {customers.bonus_programm}.<strong>Номер телефона: </strong>
              {customers.phone_number}.<br />{" "}
              <strong>Номер бонусной карты: </strong>
              {customers.bonus_card_number}.
            </div>
          </div>

          <GreenButton onClick={() => removeItem("Вы уверены?")}>
            Удалить клиента
          </GreenButton>
        </>
      ) : (
        <>
          <div className="customer__content">
            <strong>Customer ID {customers.id}.</strong>
            <div>
              <strong>Name: </strong> {customers.name}.{" "}
              <strong>Bonus programm: </strong>
              {customers.bonus_programm}.<strong>Phone namber: </strong>
              {customers.phone_number}.<br />{" "}
              <strong>Bonus card number: </strong>
              {customers.bonus_card_number}.
            </div>
          </div>

          <GreenButton onClick={() => removeItem("Are you sure?")}>
            Delete customer
          </GreenButton>
        </>
      )}
    </div>
  );
};

export default CustomersItem;
