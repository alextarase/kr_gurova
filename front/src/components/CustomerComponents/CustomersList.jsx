import React from "react";
import CustomerItem from "./CustomersItem";

const CustomersList = ({ customers }) => {
  return (
    <div>
      {customers.map((customers) => (
        <CustomerItem key={customers.id} customers={customers} />
      ))}
    </div>
  );
};

export default CustomersList;
