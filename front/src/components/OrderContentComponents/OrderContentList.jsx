import React from "react";
import OrderContentItem from "./OrderContentItem";

const OrderContentList = ({ orderContent }) => {
  return (
    <div>
      {orderContent.map((orderContent) => (
        <OrderContentItem key={orderContent.id} orderContent={orderContent} />
      ))}
    </div>
  );
};

export default OrderContentList;
