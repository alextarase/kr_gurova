import React, { useContext, useState } from "react";
import OrderService from "../../API/OrderService";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyInput from "../UI/input/MyInput";

const OrderContentForm = () => {
  const { setFetching } = useContext(AuthContext);
  const { setModal2 } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);

  const [orderContent, setOrderContent] = useState({
    number: "",
    Date_time_start_coocking: "",
    Date_time_order_done: "",
    Date_time_order_accept: "",
    Date_time_order_served: "",
    staffID: "",
    orderID: "",
    menuID: "",
  });
  async function addNewOrderContent(e) {
    e.preventDefault();
    const response = await OrderService.addOrderContent(
      orderContent.number,
      orderContent.Date_time_start_coocking,
      orderContent.Date_time_order_done,
      orderContent.Date_time_order_accept,
      orderContent.Date_time_order_served,
      orderContent.staffID,
      orderContent.orderID,
      orderContent.menuID
    );
    console.log(response);
    setFetching(true);
    setModal2();
  }

  return (
    <form>
      {lang === "RU" ? (
        <>
          <MyInput
            onChange={(e) =>
              setOrderContent({ ...orderContent, number: e.target.value })
            }
            value={orderContent.number}
            type="text"
            placeholder="Номер данных заказа"
          />
          <MyInput
            onChange={(e) =>
              setOrderContent({
                ...orderContent,
                Date_time_start_coocking: e.target.value,
              })
            }
            value={orderContent.Date_time_start_coocking}
            type="text"
            placeholder="Дата и время начала готовки в формате гггг-мм-дд чч:мм:сс"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({
                ...orderContent,
                Date_time_order_done: e.target.value,
              })
            }
            value={orderContent.Date_time_order_done}
            type="text"
            placeholder="Дата и время исполнения заказа"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({
                ...orderContent,
                Date_time_order_accept: e.target.value,
              })
            }
            value={orderContent.Date_time_order_accept}
            type="text"
            placeholder="Дата и время принятия заказа"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({
                ...orderContent,
                Date_time_order_served: e.target.value,
              })
            }
            value={orderContent.Date_time_order_served}
            type="text"
            placeholder="Дата и время подачи заказа"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({ ...orderContent, staffID: e.target.value })
            }
            value={orderContent.staffID}
            type="text"
            placeholder="staffID"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({ ...orderContent, orderID: e.target.value })
            }
            value={orderContent.orderID}
            type="text"
            placeholder="orderID"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({ ...orderContent, menuID: e.target.value })
            }
            value={orderContent.menuID}
            type="text"
            placeholder="menuID"
          />
          <GreenButton onClick={addNewOrderContent}>
            Создать данные заказа
          </GreenButton>
        </>
      ) : (
        <>
          <MyInput
            onChange={(e) =>
              setOrderContent({ ...orderContent, number: e.target.value })
            }
            value={orderContent.number}
            type="number"
            placeholder="orderContent number"
          />
          <MyInput
            onChange={(e) =>
              setOrderContent({
                ...orderContent,
                Date_time_start_coocking: e.target.value,
              })
            }
            value={orderContent.Date_time_start_coocking}
            type="text"
            placeholder="orderContent Date_time_start_coocking"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({
                ...orderContent,
                Date_time_order_done: e.target.value,
              })
            }
            value={orderContent.Date_time_order_done}
            type="text"
            placeholder="orderContent Date_time_order_done"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({
                ...orderContent,
                Date_time_order_accept: e.target.value,
              })
            }
            value={orderContent.Date_time_order_accept}
            type="text"
            placeholder="orderContent Date_time_order_accept"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({
                ...orderContent,
                Date_time_order_served: e.target.value,
              })
            }
            value={orderContent.Date_time_order_served}
            type="text"
            placeholder="orderContent Date_time_order_served"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({ ...orderContent, staffID: e.target.value })
            }
            value={orderContent.staffID}
            type="text"
            placeholder="orderContent staffID"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({ ...orderContent, orderID: e.target.value })
            }
            value={orderContent.orderID}
            type="text"
            placeholder="orderContent orderID"
          />{" "}
          <MyInput
            onChange={(e) =>
              setOrderContent({ ...orderContent, menuID: e.target.value })
            }
            value={orderContent.menuID}
            type="text"
            placeholder="orderContent menuID"
          />
          <GreenButton onClick={addNewOrderContent}>
            Create order content
          </GreenButton>
        </>
      )}
    </form>
  );
};

export default OrderContentForm;
