import React, { useContext } from "react";
import { AuthContext } from "../../context";

const OrderContentItem = ({ orderContent }) => {
  const { lang } = useContext(AuthContext);

  return (
    <div className="orderContent">
      <div className="orderContent__content">
        {lang === "RU" ? (
          <>
            <strong>№ {orderContent.id}.</strong>
            <br />
            <strong>Номер {orderContent.number}. </strong>
            <br />
            <strong>Дата заказа: </strong>
            {orderContent.Date_time_start_coocking.slice(0, 10)}.
            <div>
              <strong> Начало готовки: </strong>
              {orderContent.Date_time_start_coocking.slice(11, -5)}.
              <strong> Заказ подан: </strong>
              {orderContent.Date_time_order_served.slice(11, -5)}. <br />
              <strong> Заказ принят: </strong>
              {orderContent.Date_time_order_accept.slice(11, -5)}.
              <strong> Заказ приготовлен: </strong>
              {orderContent.Date_time_order_done.slice(11, -5)}.
            </div>
          </>
        ) : (
          <>
            <strong>ID {orderContent.id}.</strong>
            <br />
            <strong>Number {orderContent.number}.</strong>
            <br />
            <strong> Order date: </strong>
            {orderContent.Date_time_start_coocking.slice(0, 10)}.
            <div>
              <strong> Time_start_coocking: </strong>
              {orderContent.Date_time_start_coocking.slice(11, -5)}.
              <strong> Time_order_served: </strong>
              {orderContent.Date_time_order_served.slice(11, -5)}.<br />
              <strong> Time_order_accept: </strong>
              {orderContent.Date_time_order_accept.slice(11, -5)}.
              <strong> Time_order_done: </strong>
              {orderContent.Date_time_order_done.slice(11, -5)}.
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default OrderContentItem;
