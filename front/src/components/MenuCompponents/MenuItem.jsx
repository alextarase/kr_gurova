import React, { useContext } from "react";
import MenuService from "../../API/MenuService";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyModal from "../UI/MyModal/MyModal";

const MenuItem = ({ ...props }) => {
  // @ts-ignore
  const { setFetching } = useContext(AuthContext);
  // @ts-ignore
  const { lang } = useContext(AuthContext);
  const { modal3, setModal3 } = useContext(AuthContext);

  // @ts-ignore
  const removeItem = (str) => {
    const result = window.confirm(str);
    if (result) {
      MenuService.delete(props.menu.id);
      setFetching(true);
      setModal3(false);
    }
  };

  return (
    <div className="menu">
      {lang === "RU" ? (
        <>
          <div className="menu__content">
            <strong>Меню ID.{props.menu.id}</strong>
            <div>
              Имя: {props.menu.name}.
              <strong> Стоимость: {props.menu.cost}. </strong>
              Колории: {props.menu.colories}. Вес: {props.menu.weight}.
              Описание: {props.menu.description}.
            </div>
          </div>

          <GreenButton onClick={() => removeItem("Вы уверены?")}>
            Удалить блюдо
          </GreenButton>
        </>
      ) : (
        <>
          <div className="menu__content">
            <strong>Menu ID.{props.menu.id}</strong>
            <div>
              Name: {props.menu.name}.<strong>Cost: {props.menu.cost}.</strong>
              Colories: {props.menu.colories}.Weight: {props.menu.weight}.
              Description: {props.menu.description}.
            </div>
          </div>

          <GreenButton onClick={() => removeItem("Are you sure?")}>
            Delete menu Item
          </GreenButton>
        </>
      )}
    </div>
  );
};

export default MenuItem;
