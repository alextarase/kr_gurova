import React from "react";
import GroupMenuItem from "./GroupMenuItem";

const GroupMenuList = ({ groupMenu }) => {
  return (
    <div>
      {groupMenu.map((groupMenu) => (
        <GroupMenuItem key={groupMenu.id} groupMenu={groupMenu} />
      ))}
    </div>
  );
};

export default GroupMenuList;
