import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";

const GroupMenuItem = ({ ...props }) => {
  const { lang } = useContext(AuthContext);
  const navigate = useNavigate();

  async function getDishes(id) {
    navigate(`/menu_items/${id}`);
  }

  return (
    <div className="groupMenu">
      {lang === "RU" ? (
        <>
          <div className="groupMenu__content">
            <strong>ID.{props.groupMenu.id}</strong>
            <div>Раздел меню: {props.groupMenu.name}.</div>
          </div>
          <GreenButton onClick={() => getDishes(props.groupMenu.id)}>
            Получить блюда
          </GreenButton>
        </>
      ) : (
        <>
          <div className="groupMenu__content">
            <strong>ID.{props.groupMenu.id}</strong>
            <div>Menu section: {props.groupMenu.name}.</div>
          </div>
          <GreenButton onClick={() => getDishes(props.groupMenu.id)}>
            Get dishes
          </GreenButton>
        </>
      )}
    </div>
  );
};

export default GroupMenuItem;
