import React, { useContext, useState } from "react";
import MenuService from "../../API/MenuService";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyInput from "./../UI/input/MyInput";

const MenuForm = () => {
  const { fetching, setFetching } = useContext(AuthContext);
  const { modal, setModal } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);

  const [menu, setMenu] = useState({
    name: "",
    cost: "",
    colories: "",
    weight: "",
    description: "",
    groupMenu: "",
  });

  async function addNewMenu(e) {
    e.preventDefault();

    const response = await MenuService.add(
      menu.name,
      menu.cost,
      menu.colories,
      menu.weight,
      menu.description,
      menu.groupMenu
    );
    console.log(response);
    setFetching(true);
    setModal();
    setMenu({
      name: "",
      cost: "",
      colories: "",
      weight: "",
      description: "",
      groupMenu: "",
    });
  }

  return (
    <form>
      {lang === "RU" ? (
        <>
          <MyInput
            onChange={(e) => setMenu({ ...menu, name: e.target.value })}
            value={menu.name}
            type="text"
            placeholder="Название"
          />
          <MyInput
            onChange={(e) => setMenu({ ...menu, cost: e.target.value })}
            value={menu.cost}
            type="number"
            placeholder="Стоимость"
          />
          <MyInput
            onChange={(e) => setMenu({ ...menu, colories: e.target.value })}
            value={menu.colories}
            type="number"
            placeholder="Колории"
          />
          <MyInput
            onChange={(e) => setMenu({ ...menu, weight: e.target.value })}
            value={menu.weight}
            type="number"
            placeholder="Вес"
          />
          <MyInput
            onChange={(e) => setMenu({ ...menu, description: e.target.value })}
            value={menu.description}
            type="text"
            placeholder="Описание"
          />
          <MyInput
            onChange={(e) => setMenu({ ...menu, groupMenu: e.target.value })}
            value={menu.groupMenu}
            type="number"
            placeholder="Номер группы"
          />
          <GreenButton onClick={addNewMenu}>Создать блюдо</GreenButton>
        </>
      ) : (
        <>
          <MyInput
            onChange={(e) => setMenu({ ...menu, name: e.target.value })}
            value={menu.name}
            type="text"
            placeholder="Menu name"
          />
          <MyInput
            onChange={(e) => setMenu({ ...menu, cost: e.target.value })}
            value={menu.cost}
            type="number"
            placeholder="Menu cost"
          />
          <MyInput
            onChange={(e) => setMenu({ ...menu, colories: e.target.value })}
            value={menu.colories}
            type="number"
            placeholder="Menu colories"
          />
          <MyInput
            onChange={(e) => setMenu({ ...menu, weight: e.target.value })}
            value={menu.weight}
            type="number"
            placeholder="Menu weight"
          />
          <MyInput
            onChange={(e) => setMenu({ ...menu, description: e.target.value })}
            value={menu.description}
            type="text"
            placeholder="Menu description"
          />
          <MyInput
            onChange={(e) => setMenu({ ...menu, groupMenu: e.target.value })}
            value={menu.groupMenu}
            type="number"
            placeholder="Group number"
          />
          <GreenButton onClick={addNewMenu}>Create menu Item</GreenButton>
        </>
      )}
    </form>
  );
};

export default MenuForm;
