import React from "react";
import MenuItem from "./MenuItem";

const MenuList = ({ menu }) => {
  return (
    <div>
      {menu.map((menu) => (
        <MenuItem key={menu.id} menu={menu} />
      ))}
    </div>
  );
};

export default MenuList;
