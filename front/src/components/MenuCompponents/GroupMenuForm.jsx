// @ts-nocheck
import React, { useContext } from "react";
import { useState } from "react";
import MenuService from "../../API/MenuService";
import { AuthContext } from "../../context";
import GreenButton from "../UI/button/GreenButton";
import MyInput from "../UI/input/MyInput";

const GroupMenuForm = () => {
  const { fetching, setFetching } = useContext(AuthContext);
  const { modal, setModal } = useContext(AuthContext);
  const { lang } = useContext(AuthContext);
  const [groupMenu, setGroupMenu] = useState({
    name: "",
  });

  async function addNewGroupMenu(e) {
    e.preventDefault();

    const response = await MenuService.addGroupMenu(groupMenu.name);
    console.log(response);
    setFetching(true);
    setModal();
    setGroupMenu({
      name: "",
    });
    // setModal2(false);
  }
  return (
    <form>
      {lang === "RU" ? (
        <>
          <MyInput
            onChange={(e) =>
              setGroupMenu({ ...groupMenu, name: e.target.value })
            }
            value={groupMenu.name}
            type="text"
            placeholder="Название группы блюд"
          />
          <GreenButton onClick={addNewGroupMenu}>
            Создать группу блюд
          </GreenButton>
        </>
      ) : (
        <>
          <MyInput
            onChange={(e) =>
              setGroupMenu({ ...groupMenu, name: e.target.value })
            }
            value={groupMenu.name}
            type="text"
            placeholder="Group menu name"
          />
          <GreenButton onClick={addNewGroupMenu}>
            Create group menu Item
          </GreenButton>
        </>
      )}
    </form>
  );
};

export default GroupMenuForm;
