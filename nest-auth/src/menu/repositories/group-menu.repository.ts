import { EntityRepository, Repository } from 'typeorm';
import { CreateGroupMenuDto } from '../dto/create-group-menu.dto';
import { GroupMenu } from '../entities/group-menu.entity';
import { Menu } from '../entities/menu.entity';

@EntityRepository(GroupMenu)
export class GroupMenuRepository extends Repository<GroupMenu> {
  async getGroupMenu(): Promise<GroupMenu[]> {
    const query = this.createQueryBuilder('group-menu');
    const groupMenuContent = await query.getMany();
    return groupMenuContent;
  }

  async createGroupMenu(
    createGroupMenuDto: CreateGroupMenuDto,
  ): Promise<GroupMenu> {
    const { name } = createGroupMenuDto;
    const groupMenu = new GroupMenu();
    groupMenu.name = name;
    await groupMenu.save();
    return groupMenu;
  }
}
