import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GroupMenuRepository } from './repositories/group-menu.repository';
import { MenuController } from './menu.controller';
import { MenuRepository } from './repositories/menu.repository';
import { MenuService } from './menu.service';

@Module({
  imports: [TypeOrmModule.forFeature([MenuRepository, GroupMenuRepository])],
  controllers: [MenuController],
  providers: [MenuService],
})
export class MenuModule {}
