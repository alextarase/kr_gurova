import { IsString, MaxLength, MinLength } from 'class-validator';

export class CreateMenuDto {
  @IsString()
  @MinLength(3)
  @MaxLength(20)
  name: string;
  cost: number;
  colories: number;
  weight: number;
  description: string;
  groupMenu: number;
}
