import { EntityRepository, Repository } from 'typeorm';
import { OrderStatus } from '../order-status.enum';
import { Order } from '../entities/order.entity';
import { GetOrderFilterDto } from '../dto/get-order-filter.dto';
import { CreateOrderDto } from '../dto/create-order.dto';
import { Staff } from 'src/staff/staff.entity';
import { Board } from 'src/board/board.entity';
import { Customers } from 'src/customers/customers.entity';

@EntityRepository(Order)
export class OrderRepository extends Repository<Order> {
  async getOrder(filterDto: GetOrderFilterDto): Promise<Order[]> {
    const { status } = filterDto;
    const query = this.createQueryBuilder('order');

    if (status) {
      query.andWhere('order.status = :status', { status });
    }

    const order = await query.getMany();
    return order;
  }

  async createOrder(
    createOrderDto: CreateOrderDto,
    staff: Staff,
    board: Board,
    customers: Customers,
  ): Promise<Order> {
    const { name, number, payed_summ, Date_time_start, Date_time_finish } =
      createOrderDto;
    const order = new Order();
    order.name = name;
    order.number = number;
    order.payed_summ = payed_summ;
    order.status = OrderStatus.OPEN;
    order.Date_time_start = Date_time_start;
    order.Date_time_finish = Date_time_finish;
    order.staffID = staff;
    order.boardID = board;
    order.customersID = customers;
    await order.save();
    return order;
  }
}
