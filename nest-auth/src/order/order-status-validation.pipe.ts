import { BadRequestException, PipeTransform } from '@nestjs/common';
import { OrderStatus } from './order-status.enum';

export class OrderStatusValidationPipe implements PipeTransform {
  readonly allowedStatus = [
    OrderStatus.OPEN,
    OrderStatus.IN_PROGRESS,
    OrderStatus.DONE,
  ];

  transform(value: any) {
    if (!this.isStatusValid(value)) {
      throw new BadRequestException(`"${value}" is an invalid status`);
    }

    return value;
  }

  private isStatusValid(status: any) {
    const idx = this.allowedStatus.indexOf(status);
    return idx !== -1;
  }
}
