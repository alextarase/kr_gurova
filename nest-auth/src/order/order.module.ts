import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BoardRepository } from 'src/board/board.repository';
import { CustomersRepository } from 'src/customers/customers.repository';
import { MenuRepository } from 'src/menu/repositories/menu.repository';
import { StaffRepository } from 'src/staff/staff.repository';

import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { OrderContentRepository } from './repositories/order-content.repository';
import { OrderRepository } from './repositories/order.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      OrderRepository,
      StaffRepository,
      OrderContentRepository,
      BoardRepository,
      CustomersRepository,
      MenuRepository,
    ]),
  ],
  controllers: [OrderController],
  providers: [OrderService],
})
export class OrderModule {}
