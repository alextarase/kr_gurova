import { IsString, MaxLength, MinLength } from 'class-validator';

export class CreateStaffDto {
  @IsString()
  @MinLength(3)
  @MaxLength(20)
  name: string;
  @MaxLength(10)
  salary: number;
  @IsString()
  @MinLength(11)
  @MaxLength(15)
  phone_number: string;
}
