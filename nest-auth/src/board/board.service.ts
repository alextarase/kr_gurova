import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BoardOrder } from './board-order.entity';
import { BoardOrderRepository } from './board-order.repository';
import { BoardStatus } from './board-status.enum';
import { Board } from './board.entity';
import { BoardRepository } from './board.repository';
import { CreateBoardOrderDto } from './dto/create-board-order.dto';
import { CreateBoardDto } from './dto/create-board.dto';
import { GetBoardFilterDto } from './dto/get-board-filter.dto';
import { GetBoardOrderFilterDto } from './dto/get-borderOrder.filter.dto';

@Injectable()
export class BoardService {
  constructor(
    @InjectRepository(BoardRepository)
    private boardRepository: BoardRepository,
    private boardOrderRepository: BoardOrderRepository,
  ) {}

  async createBoard(createBoardDto: CreateBoardDto): Promise<Board> {
    return this.boardRepository.createBoard(createBoardDto);
  }

  async getBoard(filterDto: GetBoardFilterDto): Promise<Board[]> {
    return this.boardRepository.getBoard(filterDto);
  }

  async getBoardById(id: number): Promise<Board> {
    const found = await this.boardRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`Board with ID "${id}" not found`);
    }
    return found;
  }

  async getBoardOrder(
    filterDto: GetBoardOrderFilterDto,
  ): Promise<BoardOrder[]> {
    return this.boardOrderRepository.getBoardOrder(filterDto);
  }

  async deleteOrderById(id: number): Promise<void> {
    const result = await this.boardRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Staff with ID "${id}" not found`);
    }
  }

  async updateBoardStatus(id: number, status: BoardStatus): Promise<Board> {
    const board = await this.getBoardById(id);
    board.status = status;

    await this.boardRepository.update({ id }, board);
    return board;
  }

  async createBoardOrder(
    createBoardOrderDto: CreateBoardOrderDto,
  ): Promise<BoardOrder> {
    const board = await this.getBoardById(createBoardOrderDto.boardID);
    return this.boardOrderRepository.createBoardOrder(
      createBoardOrderDto,
      board,
    );
  }
}
