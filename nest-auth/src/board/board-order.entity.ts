import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Board } from './board.entity';

@Entity()
export class BoardOrder extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'timestamptz' })
  Date_time_ordered: Date;

  @ManyToOne(() => Board, (board) => board.id)
  @JoinColumn()
  boardID: Board;

  @Column()
  name: string;

  @Column()
  phone_number: string;

  @Column()
  sit_number: number;
}
