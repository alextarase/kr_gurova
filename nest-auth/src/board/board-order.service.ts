import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BoardOrder } from './board-order.entity';
import { BoardOrderRepository } from './board-order.repository';
import { CreateBoardOrderDto } from './dto/create-board-order.dto';

@Injectable()
export class BoardOrderService {
  constructor(
    @InjectRepository(BoardOrderRepository)
    private boardOrderRepository: BoardOrderRepository,
  ) {}

  // async createBoardOrder(
  //   createBoardOrderDto: CreateBoardOrderDto,
  // ): Promise<BoardOrder> {
  //   return this.boardOrderRepository.createBoardOrder(createBoardOrderDto);
  // }
}
