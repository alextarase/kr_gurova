import { Order } from 'src/order/entities/order.entity';
import {
  BaseEntity,
  Column,
  Entity,
  OneToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';

@Entity('customers')
@Unique(['bonus_card_number'])
export class Customers extends BaseEntity {
  @PrimaryGeneratedColumn()
  @OneToOne(() => Order, (order) => order.customersID)
  id: number;

  @Column()
  name: string;

  @Column()
  bonus_programm: number;

  @Column()
  phone_number: string;

  @Column()
  bonus_card_number: string;
}
