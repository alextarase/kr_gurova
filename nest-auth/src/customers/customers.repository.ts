import { EntityRepository, Repository } from 'typeorm';
import { Customers } from './customers.entity';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { GetCustomerDto } from './dto/get-customer.dto';

@EntityRepository(Customers)
export class CustomersRepository extends Repository<Customers> {
  async getCustomer(getCustomerDto: GetCustomerDto): Promise<Customers[]> {
    const { name, bonus_programm } = getCustomerDto;

    const query = this.createQueryBuilder('customers');

    if (name) {
      query.andWhere('customers.name = :name', { name });
    }
    if (bonus_programm) {
      query.andWhere('(customers.bonus_programm = :bonus_programm)', {
        bonus_programm,
      });
    }

    const customer = await query.getMany();
    return customer;
  }

  async createCustomer(
    createCustomerDto: CreateCustomerDto,
  ): Promise<Customers> {
    const { name, bonus_programm, phone_number, bonus_card_number } =
      createCustomerDto;

    const customer = new Customers();
    customer.name = name;
    customer.bonus_programm = bonus_programm;
    customer.phone_number = phone_number;
    customer.bonus_card_number = bonus_card_number;
    await customer.save();
    return customer;
  }
}
